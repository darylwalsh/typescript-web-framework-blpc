import { UserForm } from './views/UserForm'
import { User } from './models/User'

const user = User.buildUser({ name: 'NAME', age: 20 })

const root = document.getElementById('root')

if (root) {
  const userForm = new UserForm(root, user)

  userForm.render()
} else {
  throw new Error('Root element not found')
}
// import { User, UserProps } from './models/User'
// import { Collection } from './models/Collection'

// const collection = new Collection<User, UserProps>(
//   'http://localhost:3000/users',
//   (json: UserProps) => User.buildUser(json),
// )

// collection.on('change', () => {
//   console.log(collection)
// })

// collection.fetch()

// import { User } from './models/User'

// const user = new User({ name: 'newish users', age: 44 })
// const user = User.buildUser({ id: 11 })

// console.log(user.get('name'))

// const on = user.on

// user.on('change', () => {
//   console.log(user)
// })
// user.fetch()

// user.fetch()
// user.set({ name: 'Update Update' })

// console.log(user.get('name'))
// user.set({ name: 'NEW NAME' })
// user.set({ age: 27 })

// user.save()

// user.fetch()

// setTimeout(() => {
//   console.log(user)
// }, 4000)

// axios.post('http://localhost:3000/users', {
//   name: 'myname',
//   age: 20,
// })

// axios.get('http://localhost:3000/users/4')

// import { User } from './models/User'

// const user = new User({ name: 'myname', age: 20 })

// user.set({ name: 'agenewname', age: 9999 })

// user.on('change', () => {
//   console.log('userChange #1user')
// })
// user.on('change', () => {
//   console.log('console# CHange 3use2')
// })
// user.on('save', () => {
//   console.log('userSave triggered')
// })

// user.trigger('change')
// user.trigger('save')
// console.log(user)

// const name = user.get('name')
// const age = user.get('age')

// console.log(name)
// console.log(age)
