# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.1.17](https://gitlab.com/darylwalsh/typescript-web-framework-blpc/compare/v0.1.16...v0.1.17) (2019-09-13)


### Features

* **user_save:** add user save view ([203f424](https://gitlab.com/darylwalsh/typescript-web-framework-blpc/commit/203f424))

### [0.1.16](https://gitlab.com/darylwalsh/typescript-web-framework-blpc/compare/v0.1.15...v0.1.16) (2019-09-10)


### Features

* **view_class:** add generic view class ([52e477e](https://gitlab.com/darylwalsh/typescript-web-framework-blpc/commit/52e477e))

### [0.1.15](https://gitlab.com/darylwalsh/typescript-web-framework-blpc/compare/v0.1.14...v0.1.15) (2019-09-10)


### Features

* **add userform view:** added view for UserForm ([d75ffc4](https://gitlab.com/darylwalsh/typescript-web-framework-blpc/commit/d75ffc4))

### [0.1.14](https://gitlab.com/darylwalsh/typescript-web-framework-blpc/compare/v0.1.13...v0.1.14) (2019-09-09)


### Features

* **complete_api:** completed back end code for TS based generic data ([37a49fe](https://gitlab.com/darylwalsh/typescript-web-framework-blpc/commit/37a49fe))

### [0.1.13](https://gitlab.com/darylwalsh/typescript-web-framework-blpc/compare/v0.1.12...v0.1.13) (2019-09-05)


### Features

* **user_model:** checkpoint for User conversion to generic Model ([684b8b1](https://gitlab.com/darylwalsh/typescript-web-framework-blpc/commit/684b8b1))

### [0.1.12](https://gitlab.com/darylwalsh/typescript-web-framework-blpc/compare/v0.1.11...v0.1.12) (2019-09-04)


### Features

* **user_class:** implement all user methods via composition ([7896800](https://gitlab.com/darylwalsh/typescript-web-framework-blpc/commit/7896800))

### [0.1.11](https://gitlab.com/darylwalsh/typescript-web-framework-blpc/compare/v0.1.10...v0.1.11) (2019-09-04)

### [0.1.10](https://gitlab.com/darylwalsh/typescript-web-framework-blpc/compare/v0.1.9...v0.1.10) (2019-09-04)


### Features

* **user_class:** add helper methods to  User class ([67a0a82](https://gitlab.com/darylwalsh/typescript-web-framework-blpc/commit/67a0a82))

### [0.1.9](https://gitlab.com/darylwalsh/typescript-web-framework-blpc/compare/v0.1.8...v0.1.9) (2019-09-03)

## [0.1.8](https://gitlab.com/darylwalsh/typescript-web-framework-blpc/compare/v0.1.6...v0.1.8) (2019-09-03)



## [0.1.6](https://gitlab.com/darylwalsh/typescript-web-framework-blpc/compare/v0.1.5...v0.1.6) (2019-09-03)



## [0.1.5](https://gitlab.com/darylwalsh/typescript-web-framework-blpc/compare/v0.1.4...v0.1.5) (2019-09-03)



## [0.1.4](https://gitlab.com/darylwalsh/typescript-web-framework-blpc/compare/v0.1.2...v0.1.4) (2019-09-03)


### Features

* **user_sync:** created user and sync classes ([e8d8b63](https://gitlab.com/darylwalsh/typescript-web-framework-blpc/commit/e8d8b63))



## [0.1.2](https://gitlab.com/darylwalsh/typescript-web-framework-blpc/compare/a5de29f...v0.1.2) (2019-09-03)


### Features

* **user_sync:** create user and sync classes ([a5de29f](https://gitlab.com/darylwalsh/typescript-web-framework-blpc/commit/a5de29f))
